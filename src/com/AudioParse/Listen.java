package com.AudioParse;

import android.app.Activity;
import android.app.AlertDialog;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import com.example.AudioParse.R;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Queue;

/**
 * Created by Ish on 2/8/14.
 */
public class Listen extends Activity implements View.OnClickListener {


    private static final int SIZE = 4096;
    ImageButton record;
    TextView note;
    private boolean isRecording = false;


    private static final int RECORDER_SAMPLERATE = 16000;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    private AudioRecord recorder;
    private Thread recordingThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listen);
        record = (ImageButton)findViewById(R.id.record);
        record.setOnClickListener(this);

        note = (TextView)findViewById(R.id.note);

        recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                RECORDER_SAMPLERATE, RECORDER_CHANNELS,
                RECORDER_AUDIO_ENCODING, SIZE);
    }
    public void onClick(View v){

        int vid = v.getId();
        if(vid == record.getId()){

            if(!isRecording){

                startRecording2();

            }

            else{

                stopRecording();

            }

        }
    }

/*

    private void startRecording() {

        recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                RECORDER_SAMPLERATE, RECORDER_CHANNELS,
                RECORDER_AUDIO_ENCODING, BufferElements2Rec * BytesPerElement);

        recorder.startRecording();
        isRecording = true;
        recordingThread = new Thread(new Runnable() {
            public void run() {
                writeAudioDataToFile();
            }
        }, "AudioRecorder Thread");
        recordingThread.start();
    }

    //convert short to byte
    private byte[] short2byte(short[] sData) {
        int shortArrsize = sData.length;
        byte[] bytes = new byte[shortArrsize * 2];
        for (int i = 0; i < shortArrsize; i++) {
            bytes[i * 2] = (byte) (sData[i] & 0x00FF);
            bytes[(i * 2) + 1] = (byte) (sData[i] >> 8);
            sData[i] = 0;
        }
        return bytes;

    }

    private void writeAudioDataToFile() {
        // Write the output audio in byte

        String filePath = "/sdcard/voice8K16bitmono.pcm";
        short sData[] = new short[BufferElements2Rec];



        FileOutputStream os = null;
        try {
            os = new FileOutputStream(filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        while (isRecording) {
            // gets the voice output from microphone to byte format

            recorder.read(sData, 0, BufferElements2Rec);
            System.out.println("Short wirting to file" + sData.toString());
            try {
                // // writes the data to file from buffer
                // // stores the voice buffer
                byte bData[] = short2byte(sData);
                os.write(bData, 0, BufferElements2Rec * BytesPerElement);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
*/
    private void stopRecording() {
        // stops the recording activity
        if (null != recorder) {
            isRecording = false;
            recorder.stop();

            try{

                recordingThread.join();
            }catch(Exception e){
                e.printStackTrace();
            }

        }
    }

    private void startRecording2() {

        final ByteBuffer bData = ByteBuffer.allocate(SIZE);

        recorder.startRecording();
        isRecording = true;
        recordingThread = new Thread(new Runnable() {

            @Override
            public void run() {
                while (isRecording) {

                    int result = recorder.read(bData, SIZE);
                    System.out.println("READ DATA");


                    if (result > 0) {
                        AlertDialog alertDialog = new AlertDialog.Builder(Listen.this).create();
                        alertDialog.setTitle("Alert ");
                        alertDialog.setMessage("Success");
                        alertDialog.setCanceledOnTouchOutside(true);
                        alertDialog.show();
                        //ANALYSIS GOES HERE
                    } else if (result == AudioRecord.ERROR_INVALID_OPERATION) {
                        Log.e("Recording", "Invalid operation error");

                        break;
                    } else if (result == AudioRecord.ERROR_BAD_VALUE) {
                        Log.e("Recording", "Bad value error");
                        break;
                    } else if (result == AudioRecord.ERROR) {
                        Log.e("Recording", "Unknown error");
                        break;
                    }

                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        break;
                    }
                }
            }
        }, "AudioRecorder Thread");
        recordingThread.start();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
