package com.AudioParse;

import com.leff.midi.MidiFile;
import com.leff.midi.MidiTrack;
import com.leff.midi.event.meta.Tempo;
import edu.emory.mathcs.jtransforms.fft.DoubleFFT_1D;

import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Mayank on 2/8/14.
 */
public class AudioParser
{
    public static String parse(double[] buffer, int sampleRate)
    {
        return getNoteNames(getPitches(buffer, sampleRate));
    }

    private static int frequencyToMidi(double frequency)
    {
        return (int)(12 * Math.log(frequency / 440) / Math.log(2)) + 69;
    }

    private static String noteNumberToLetter(int note)
    {
        switch (note)
        {
            case 0: return "C";
            case 1: return "C#";
            case 2: return "D";
            case 3: return "D#";
            case 4: return "E";
            case 5: return "F";
            case 6: return "F#";
            case 7: return "G";
            case 8: return "G#";
            case 9: return "A";
            case 10: return "A#";
            case 11: return "B";
            default: return "?";
        }
    }

    private static double[] getPitches(double[] buffer, int sampleRate)
    {
        final int LENGTH = buffer.length;

        DoubleFFT_1D fft = new DoubleFFT_1D(LENGTH);
        double[] fftBuffer = new double[LENGTH * 2];
        System.arraycopy(buffer, 0, fftBuffer, 0, LENGTH);
        fft.realForwardFull(fftBuffer);

        double[] magnitudes = new double[LENGTH];
        for (int i = 0; i < 2 * LENGTH; i += 2)
        {
            double real = fftBuffer[i];
            double imag = fftBuffer[i + 1];
            magnitudes[i] = Math.sqrt(real * real + imag * imag);
        }

        double[] pitches = new double[128];
        for (int i = 0; i < LENGTH; ++i)
        {
            int pitch = frequencyToMidi(sampleRate * i / LENGTH);
            pitches[pitch] += magnitudes[i];
        }

        return pitches;
    }

    private static String getNoteNames(double[] pitches)
    {
        double[] notes = new double[12];
        for (int i = 0; i < 128; ++i)
        {
            notes[i % 12] += pitches[i];
        }

        int max1 = -1, max2 = -1, max3 = -1;
        for (int i = 0; i < 12; ++i)
        {
            if (max1 < 0 || notes[i] >= notes[max1])
            {
                max3 = max2;
                max2 = max1;
                max1 = i;
            }
            else if (max2 < 0 || notes[i] >= notes[max2])
            {
                max3 = max2;
                max2 = i;
            }
            else if (max3 < 0 || notes[i] >= notes[max3])
            {
                max3 = i;
            }
        }

        int[] maxes = {max1, max2, max3};
        Arrays.sort(maxes);

        return noteNumberToLetter(maxes[0]) + noteNumberToLetter(maxes[1]) + noteNumberToLetter(maxes[2]);
    }

    private static String getChord(String notes)
    {
        return "?";
    }
}
